package com.example.smartcityone;

public class apter {


    /**
     * ResultObj : {"UserID":122771,"UserName":"","Email":"","Telphone":"13915555001","Gender":true,"CollegeID":0,"CollegeName":"","RoleName":"普通会员","RoleID":15,"AccessToken":"A1291E2A341BE47824630B7DBC0C3F87961832EBDC494CA1B8DD490EA114AEBE61B42DF55114C0844BD0B8F952EA052E1A45889C4A4DA218EF50052DF197BB8D80090D91AD708D3E5324250E83A3ED5613ABC8DAE096AF7C8CAA21CB1C0C599551BC8C9812F66657F75F7AF9795A06FDA31D0AD86EB7CB0ACEB3879D43F0837B119804B017A286BBD7676EFC342C203989718496C46B7D04B0E6761861F9FF87BD5856F83C056955C5CA6F18C8032010B6D8FB77BD55874D1AA674EF28C04FC86C609D67F5D4DDEFB6E56C75F4BE7C64CCDD4533F2B782558FB6F0A39A67F862","AccessTokenErrCode":0,"ReturnUrl":null,"DataToken":"23713f195e704be7"}
     * Status : 0
     * StatusCode : 0
     * Msg : null
     * ErrorObj : null
     */

    private ResultObjBean ResultObj;
    private int Status;
    private int StatusCode;
    private Object Msg;
    private Object ErrorObj;

    public ResultObjBean getResultObj() {
        return ResultObj;
    }

    public void setResultObj(ResultObjBean ResultObj) {
        this.ResultObj = ResultObj;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public Object getMsg() {
        return Msg;
    }

    public void setMsg(Object Msg) {
        this.Msg = Msg;
    }

    public Object getErrorObj() {
        return ErrorObj;
    }

    public void setErrorObj(Object ErrorObj) {
        this.ErrorObj = ErrorObj;
    }

    public static class ResultObjBean {
        /**
         * UserID : 122771
         * UserName :
         * Email :
         * Telphone : 13915555001
         * Gender : true
         * CollegeID : 0
         * CollegeName :
         * RoleName : 普通会员
         * RoleID : 15
         * AccessToken : A1291E2A341BE47824630B7DBC0C3F87961832EBDC494CA1B8DD490EA114AEBE61B42DF55114C0844BD0B8F952EA052E1A45889C4A4DA218EF50052DF197BB8D80090D91AD708D3E5324250E83A3ED5613ABC8DAE096AF7C8CAA21CB1C0C599551BC8C9812F66657F75F7AF9795A06FDA31D0AD86EB7CB0ACEB3879D43F0837B119804B017A286BBD7676EFC342C203989718496C46B7D04B0E6761861F9FF87BD5856F83C056955C5CA6F18C8032010B6D8FB77BD55874D1AA674EF28C04FC86C609D67F5D4DDEFB6E56C75F4BE7C64CCDD4533F2B782558FB6F0A39A67F862
         * AccessTokenErrCode : 0
         * ReturnUrl : null
         * DataToken : 23713f195e704be7
         */

        private int UserID;
        private String UserName;
        private String Email;
        private String Telphone;
        private boolean Gender;
        private int CollegeID;
        private String CollegeName;
        private String RoleName;
        private int RoleID;
        private String AccessToken;
        private int AccessTokenErrCode;
        private Object ReturnUrl;
        private String DataToken;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getTelphone() {
            return Telphone;
        }

        public void setTelphone(String Telphone) {
            this.Telphone = Telphone;
        }

        public boolean isGender() {
            return Gender;
        }

        public void setGender(boolean Gender) {
            this.Gender = Gender;
        }

        public int getCollegeID() {
            return CollegeID;
        }

        public void setCollegeID(int CollegeID) {
            this.CollegeID = CollegeID;
        }

        public String getCollegeName() {
            return CollegeName;
        }

        public void setCollegeName(String CollegeName) {
            this.CollegeName = CollegeName;
        }

        public String getRoleName() {
            return RoleName;
        }

        public void setRoleName(String RoleName) {
            this.RoleName = RoleName;
        }

        public int getRoleID() {
            return RoleID;
        }

        public void setRoleID(int RoleID) {
            this.RoleID = RoleID;
        }

        public String getAccessToken() {
            return AccessToken;
        }

        public void setAccessToken(String AccessToken) {
            this.AccessToken = AccessToken;
        }

        public int getAccessTokenErrCode() {
            return AccessTokenErrCode;
        }

        public void setAccessTokenErrCode(int AccessTokenErrCode) {
            this.AccessTokenErrCode = AccessTokenErrCode;
        }

        public Object getReturnUrl() {
            return ReturnUrl;
        }

        public void setReturnUrl(Object ReturnUrl) {
            this.ReturnUrl = ReturnUrl;
        }

        public String getDataToken() {
            return DataToken;
        }

        public void setDataToken(String DataToken) {
            this.DataToken = DataToken;
        }
    }
}
